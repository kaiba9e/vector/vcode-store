#!/usr/bin/python3
#
# Simple Selection Sort (s3sort). For blog, please go to:
# o https://www.cnblogs.com/idorax/p/6537412.html
#
"""
/*
 * Simple Selection Sort
 *
 * NOTES:
 *    The simple selection sort improves on the bubble sort by making only one
 *    exchange for every pass through the list. In order to do this,
 *    a selection sort looks for the smallest value as it makes a pass and,
 *    after completing the pass, places it in the proper location.  After the
 *    first pass, the smallest item is in the correct place. After the second
 *    pass, the next smallest is in place. This process continues and requires
 *    n-1 passes to sort n items, since the final item must be in place after
 *    the (n-1) st pass.
 *
 * REFERENCES:
 *    1. https://www.cs.usfca.edu/~galles/visualization/ComparisonSort.html
 *    2. https://interactivepython.org/courselib/static/pythonds/SortSearch/\
 *	   TheSelectionSort.html
 */

static void
exchange(int a[], int i, int j)
{
    int t = a[i];
    a[i] = a[j];
    a[j] = t;
}

static int
getIndexOfMin(int a[], size_t n)
{
    int min = 0;
    for (int i = 0; i < n; i++)
        if (a[i] < a[min])
            min = i;
    return min;
}

/*
 * Simple Selection Sort (s2sort in short)
 *
 * NOTE:
 *    a[0 .. i] is always sorted and
 *    a[i+1 .. n-1] is unsorted
 */
void
s2sort(int a[], size_t n)
{
    for (int i = 0; i < n; i++) {
        int min = getIndexOfMin(a+i, n-i) + i;
        if (i != min) {
            exchange(a, i, min);
        }
    }
}
"""

import sys


def get_index_of_min(l):
    # print('MINL', l)
    n = len(l)
    idx_min = 0
    i = 0
    while i < n:
        if l[i] < l[idx_min]:
            idx_min = i
        i += 1
    return idx_min


def s3sort(l):
    n = len(l)
    i = 0
    while i < n:
        idx_min = i + get_index_of_min(l[i:])
        # print(i, l, "idx_min = %d" % idx_min, l)
        if idx_min != i:
            l[i], l[idx_min] = l[idx_min], l[i]
        i += 1
    return l


def main(argc, argv):
    if argc < 2:
        print("Usage: %s <...>" % argv[0], file=sys.stderr)
        return 1

    l = argv[1:]
    print("Before sorting: ", l)
    s3sort(l)
    print("After  sorting: ", l)
    return 0


if __name__ == '__main__':
    sys.exit(main(len(sys.argv), sys.argv))
