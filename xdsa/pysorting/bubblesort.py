#!/usr/bin/python3
#
# Bubble Sort (bsort). For blog, please go to:
# o https://www.cnblogs.com/idorax/p/6537486.html
#
"""
/*
 * Bubble Sort
 *
 * NOTES:
 *	The bubble sort makes multiple passes through a list. It compares
 *	adjacent items and exchanges those that are out of order. Each pass
 *	through the list places the next largest value in its proper place.
 *	In essence, each item "bubbles" up to the location where it belongs.
 *
 * REFERENCES:
 *	1. https://www.cs.usfca.edu/~galles/visualization/ComparisonSort.html
 *	2. https://interactivepython.org/courselib/static/pythonds/SortSearch/\
 *	   TheBubbleSort.html
 */
static void
exchange(int a[], int i, int j)
{
    int t = a[i];
    a[i] = a[j];
    a[j] = t;
}

void
bubblesort(int a[], size_t n)
{
    for (int i = 0; i < n - 1; i++) {
        for (int j = 0; j < n - 1 - i; j++) {
            if (a[j+1] < a[j]) {
                exchange(a, j+1, j);
            }
        }
    }
}
"""

import sys


def swap(a, b):
    return (b, a)


def bubblesort(l):
    n = len(l)
    i = 0
    while i < n - 1:
        j = 0
        while j < n - 1 - i:
            if l[j + 1] < l[j]:
                l[j + 1], l[j] = swap(l[j + 1], l[j])
            j += 1
        i += 1
    return l


def main(argc, argv):
    if argc < 2:
        print("Usage: %s <...>" % argv[0], file=sys.stderr)
        return 1

    l = argv[1:]
    print("Before sorting: ", l)
    bubblesort(l)
    print("After  sorting: ", l)
    return 0


if __name__ == '__main__':
    sys.exit(main(len(sys.argv), sys.argv))
