#
# Library functions which are used by all test*.sh
#

function print   { printf -- "$*\n"; }
function tolower { printf -- "$*\n" | tr 'A-Z' 'a-z'; }
function toupper { printf -- "$*\n" | tr 'a-z' 'A-Z'; }

#
# Functions to create colorful string
#
function _isatty
{
	typeset isatty=$(tolower $ISATTY)
	[[ $isatty == "yes" ]] && return 0
	[[ $isatty ==  "no" ]] && return 1
	[[ -t 1 && -t 2 ]] && return 0 || return 1
}
function str2gray    { _isatty && print "\033[1;30m$@\033[m" || print "$@"; }
function str2red     { _isatty && print "\033[1;31m$@\033[m" || print "$@"; }
function str2green   { _isatty && print "\033[1;32m$@\033[m" || print "$@"; }
function str2yellow  { _isatty && print "\033[1;33m$@\033[m" || print "$@"; }
function str2blue    { _isatty && print "\033[1;34m$@\033[m" || print "$@"; }
function str2magenta { _isatty && print "\033[1;35m$@\033[m" || print "$@"; }
function str2cyan    { _isatty && print "\033[1;36m$@\033[m" || print "$@"; }
function str2white   { _isatty && print "\033[1;37m$@\033[m" || print "$@"; }

function msg_info { str2green "[ INFO ] $@";     }
function msg_pass { str2cyan  "[ PASS ] $@";     }
function msg_fail { str2red   "[ FAIL ] $@" >&2; }

function _initPS4
{
	export PS4='__DEBUG__: [${FUNCNAME}@${BASH_SOURCE}:${LINENO}|${SECONDS}]+ '
}

function DEBUG
{
	typeset -l s=$DEBUG
	[[ $s == "yes" || $s == "true" ]] && _initPS4 && set -x
}

function run_cmd
{
	typeset prompt=$(str2yellow $(id -un)@$(hostname)\$)
	echo "$prompt $(str2cyan $*)"
	eval "$*"
	rc=$?
	echo "$prompt"
	return $rc
}
