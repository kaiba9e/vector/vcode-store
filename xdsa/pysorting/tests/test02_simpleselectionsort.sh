#!/bin/bash

FILE=$(readlink -f $BASH_SOURCE)
NAME=$(basename $FILE)
CDIR=$(dirname $FILE)
TMPDIR=${TMPDIR:-"/tmp"}

source $CDIR/libtests.sh

SORT_BIN=simpleselectionsort
function run_sort
{
	typeset func=$FUNCNAME
	export PATH=$CDIR/../:$PATH

	run_cmd $SORT_BIN 1 2 3 4 5 6 7 8 9
	run_cmd $SORT_BIN 9 8 7 6 5 4 3 2 1
	run_cmd $SORT_BIN 1 3 5 6 2 4 7 8 9
	run_cmd $SORT_BIN 1 3 2 4 2 4 3 5 1

	run_cmd $SORT_BIN a b c d e f g h i
	run_cmd $SORT_BIN i h g f e d c b a
	run_cmd $SORT_BIN a b g f d c i f e
	run_cmd $SORT_BIN a b d c b a e f a
}

DEBUG

run_sort

exit 0
